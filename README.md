# Oregon FELIX Setup

Configuration files and utility scripts for the UO test stand & FELIX upgrade

# Quick Start

## Components

Ensure the following is installed

### YARR devel

Code & installation instructions: [https://gitlab.cern.ch/YARR/YARR/-/tree/devel](https://gitlab.cern.ch/YARR/YARR/-/tree/devel)

### FELIX Software, Firmware and Driver

The SW, FW and driver need to be mutually compatible. Check the recommendations at central FELIX website: [https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/releases.html](https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/releases.html). Compatibility puzzles guaranteed.

Individual repositories & installation instructions:

FELIX distribution (aka FELIX software): [https://gitlab.cern.ch/atlas-tdaq-felix/felix-distribution/-/tree/master?ref_type=heads](https://gitlab.cern.ch/atlas-tdaq-felix/felix-distribution/-/tree/master?ref_type=heads)

FELIX firmware: [https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/firmware/5.0/](https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/firmware/5.0/) install according to [https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/felix-user-manual/versions/4.0.6/4_firmware_programming.html#sec:firmware](https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/felix-user-manual/versions/4.0.6/4_firmware_programming.html#sec:firmware)

FELIX driver: download directly from the link in [https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/releases.html](https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/releases.html), install according to [https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/felix-user-manual/versions/4.0.6/5_software_installation.html#_5_2_1_driver_rpm_installation_instructions](https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/felix-user-manual/versions/4.0.6/5_software_installation.html#_5_2_1_driver_rpm_installation_instructions)

Rule of thumb: driver will need to be installed O(1) times, firmware updated/flashed bit more often, software installed and updated a lot more often.

# Resources

Detailed instructions on YARR + FELIX (+ OPTOBOARD) by L. Nosler and A. Rastogi: [https://atlaswiki.lbl.gov/pixels/felixdaq](https://atlaswiki.lbl.gov/pixels/felixdaq)
