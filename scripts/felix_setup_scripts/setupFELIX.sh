#! /bin/bash

#reset FELIX
flx-reset -c 0 ALL

#init FELIX
flx-init -c 0

#invert Rx polarity
fgpolarity -c 0 -r set

#set FEC12 encoding
flx-config -d 0 set LPGBT_FEC=0xF

#configure link encoding/decoding
source ~/bin/config_encoding_decoding.sh 0
