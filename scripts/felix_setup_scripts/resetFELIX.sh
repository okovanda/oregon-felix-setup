#! /bin/bash
CARD="${1:-0}"

#reset all registers
flx-reset -c $CARD ALL

#reset clock
flx-init -c $CARD

#set correct RX polarity
fgpolarity -c $CARD -r set

#set FEC12 encoding
flx-config -d $CARD set LPGBT_FEC=0xF

#set epath structure
source ~/bin/config_encoding_decoding.sh $CARD

#set trigger sequences
#source ~/bin/config_caltrigseq.sh 3 $CARD
